<?php

// config

$db_host = 'localhost';
$db_user = 'root';
$db_pass = 'root';
$db_name = 'magento';

$app = new Main($db_host, $db_user, $db_pass, $db_name);

/**
 * Class Main
 */
class Main
{
    /**
     * success response
     */
    const SUCCESS = 1;

    /**
     * error response
     */
    const ERROR = 0;

    /**
     * route var name
     */
    const ROUTE_NAME = 'method';

    /**
     * @var mysqli
     */
    private $_db;

    /**
     * @var array
     */
    private $_params = array();

    /**
     * @var boolean
     */
    private $_debug = false;

    /**
     * @var string
     */
    private $_format = 'json';

    /**
     * constructor
     *
     * @param string $db_host mysql host name
     * @param string $db_user mysql user name
     * @param string $db_pass mysql password
     * @param string $db_name mysql database name
     */
    public function __construct($db_host, $db_user, $db_pass, $db_name)
    {
        $this->_db = new mysqli($db_host, $db_user, $db_pass, $db_name);
        $this->_params = $_GET;

        if (!$this->authenticate()) {
            $this->response(self::ERROR, 'invalid api key');
            exit;
        }

        $route = $this->getParam(self::ROUTE_NAME) . 'Action';

        if (method_exists($this, $route)) {
            $this->$route();
        } else {
            $this->noRoute();
        }
    }


    /**
     * install
     *
     * @return null
     */
    public function install()
    {
        $sql = "create table if not exists my_api_user (
                    user_id int(20) not null auto_increment,
                    name varchar(20),
                    password varchar(20),
                    email varchar(20) not null,
                    primary key(user_id));";

        $this->_db->query($sql);

        $sql = "create table if not exists my_api_key (
                    key_id int(20) not null auto_increment,
                    user_id varchar(20) not null,
                    api_key varchar(20),
                    permission text not null,
                    primary key(key_id));";

        $this->_db->query($sql);

        // test user
        $sql = "insert into my_api_user (user_id,name,password,email) values (1,'test','','test@test.com')";
        $this->_db->query($sql);

        $sql = "insert into my_api_key (user_id,api_key,permission) values (1,'test_key','all')";
        $this->_db->query($sql);

    }

    /**
     * get parameter
     *
     * @param string $key parameter key
     *
     * @return mixed
     */
    public function getParam($key)
    {
        return (isset($this->_params[$key])) ? $this->_params[$key] : null;
    }

    /**
     * no route action
     *
     * @return null
     */
    public function noRoute()
    {
        $this->response(self::ERROR, 'no route');
    }

    /**
     * build response
     *
     * @param integer $success response code
     * @param string  $message message
     * @param array   $data    data
     *
     * @return null
     */
    public function response($success, $message, $data = array())
    {
        $format = $this->getParam('format') ?
            $this->getParam('format') :
            $this->_format;

        $response = array(
            'success' => $success,
            'message' => $message,
            'data' => $data
        );

        header('Cache-Control: no-cache, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');

        switch ($format) {
        case 'json':
            header('Content-type: application/json');
            echo json_encode($response);
            break;

        case 'xml':
            header('Content-type: application/xml');
            $xml = new SimpleXMLElement('<response/>');
            $this->_arrayToXml($response, $xml);
            echo $xml->asXML();
            break;
        default:
            echo 'invalid format!';
            break;
        }

    }

    /**
     * authenticate
     *
     * @return bool
     */
    public function authenticate()
    {
        $key = $this->getParam('key');
        $sql = "select * from my_api_key where api_key='{$key}'";
        $data = $this->fetch($sql);
        return count($data) > 0;
    }

    /**
     * fetch sql query
     *
     * @param string $sql query
     *
     * @return mixed
     */
    public function fetch($sql)
    {
        $result = $this->_db->query($sql);
        $data = $result->fetch_all(MYSQLI_ASSOC);
        return $data;
    }

    /**
     * array to xml recursive
     *
     * @param array            $array data
     * @param SimpleXMLElement &$xml xml
     *
     * @return mixed
     */
    private function _arrayToXml($array, &$xml)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subNode = $xml->addChild("$key");
                    $this->_arrayToXml($value, $subNode);
                } else {
                    $subNode = $xml->addChild("item_$key");
                    $this->_arrayToXml($value, $subNode);
                }
            } else {
                $xml->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }

    /**
     * sample action
     *
     * @return null
     */
    public function testAction()
    {
        $filter = $this->getParam('filter');
        $where = array(1);

        if (is_array($filter)) {
            foreach ($filter as $key => $value) {
                $where[] = "{$key}='{$value}'";
            }
        }
        $sql = 'select * from core_config_data where ' . implode(' and ', $where);

        $this->response(self::SUCCESS, 'ok', $this->fetch($sql));
    }

}
